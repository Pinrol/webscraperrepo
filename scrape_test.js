var request = require('request');
var cheerio = require('cheerio');
var json2xls = require('json2xls');
// var XlsExport = require('./node_modules/xlsexport/xls-export.js');
var fs = require('fs');
// var Blob = require('blob');

var searchedNames = [];

var leoVegasGames = [];
var casumoGames = [];
var paddyPowerGames = [];
var pokerstars = [];
var speedyCasino = [];
var svenskaSpel = [];
var unibet = [];
var wonderino = [];

requestLeovegas();

function requestLeovegas() {
	request('https://www.leovegas.com/sv-se/', function (error, response, body) {
		if (error) {
			console.log('Error: ' + error);
			return;
		}
		console.log('Status code: ' + response.statusCode);

		var $ = cheerio.load(body);

		var arr = [];
		var f = $('div').each(function (index, element) {
			if ($(this).hasClass('zZp1V _3lCMv')) {
				arr.push(String($(this).text()));
			}
		});

		for (var i = 0; i < 20; i++) {
			console.log(arr[i]);
		}

		const leovegasPopularMax = 20
		for (var i = 0; i < leovegasPopularMax; i++) {
			let last = false;
			if (i === leovegasPopularMax - 1) {
				last = true;
			}
			startScrape(arr[i], leoVegasGames, i, last);
		}
	});
}

function startScrape(name, siteArray, index, last) {
	setTimeout(() => {
		scrapeSlotCatalog(name, siteArray, last);
	}, 1000 * index);
}

function scrapeSlotCatalog(slotName, siteArray, last) {
	if ((searchedNames.includes(slotName), 0)) {
		return;
	}

	var formatedString = slotName.replace(/(MEGAWAYS)(Megaways)(megaways)/g, '');
	formatedString = formatedString.replace(/\s$/g, '');
	formatedString = formatedString.replace(/\s+/g, '+');
	formatedString = formatedString.replace(/\'/g, '');
	console.log(formatedString);

	request(
		'https://admin.slotcatalog.com/index.php?id=344&translit=Search-results&lang=en&tag=SEARCH&type_id=&brand_id=&name=' +
		formatedString +
		'&RTP=&variance_risk=&layout=&betway=&min_bet=&max_bet=&max_coin_win=&short=',
		function (error, response, body) {
			if (error) {
				console.error('Error: ', error);
				return;
			}
			console.log('Status code: ', response.statusCode);

			console.log('--------------------------------------------------------------------------------------------------------');
			var $ = cheerio.load(body);
			var arr2 = [];
			var elementArray = [];
			$('a').each(function (index, element) {
				if ($(this).hasClass('good_link')) {
					arr2.push($(this).attr('href'));
				}
			});

			console.log('--------------------------------------------------------------------------------------------------------');
			let href = '';
			if (arr2.length > 1) {
				let lowestSlotRankIndex = arr2.length - 1;
				let lowestSlotRank = 100000;
				$(".widgetSRBIG-small").each((index, element) => {
					const data = element.children[0].data;
					if (data !== "N/A") {
						if (parseInt(data) < parseInt(lowestSlotRank)) {
							lowestSlotRankIndex = index;
							lowestSlotRank = data;
						}
					}
				});
				href = arr2[lowestSlotRankIndex];
			} else {
				href = arr2[0];
			}


			for (var i = 0; i < elementArray.length; i++) {
				console.log('elementList[', i, ']', elementArray[i]);
			}

			console.log(arr2);
			console.log('--------------------------------------------------------------------------------------------------------');
			openSlotCatalogGame(href, slotName, siteArray, last);
		},
	);
}

function openSlotCatalogGame(localLink, gameName, siteArray, last) {
	request('https://admin.slotcatalog.com' + localLink, function (error, response, body) {
		if (error) {
			console.error('Error: ', error);
			return;
		}
		console.log('Status code: ', response.statusCode);

		var $ = cheerio.load(body);

		const gameObject = {};
		gameObject.name = gameName;

		$('.newReviewTopAttr span').each(function (index, element) {
			if ($(this).hasClass('RTP_val')) {
				gameObject.rtp = $(this).text();
			}

			if ($(this).hasClass('variance_risk_val')) {
				gameObject.variance = $(this).text();
			}

			if ($(this).hasClass('layout_val')) {
				gameObject.layout = $(this).text();
			}

			if ($(this).hasClass('betway_val')) {
				gameObject.betways = $(this).text();
			}
		});

		let addedBothway = false;
		$('.newReviewTopAttr div').each(function (index, element) {
			if (
				$(this)
				.text()
				.includes('(Bothway -Yes!)') &&
				!addedBothway
			) {
				addedBothway = true;
				gameObject.betways += ' (Bothway -Yes!)';
			}
		});

		const tags = [];
		$(".tegs_slot_item").find('a').each((index, element) => {
			tags.push(element.children[0].data);
		});

		gameObject.tags = tags;

		console.log(gameObject);
		siteArray.push(gameObject);

		if (last) {
			var xls = json2xls(siteArray);
			fs.writeFileSync('data.xlsx', xls, 'binary');
		}
	});
}